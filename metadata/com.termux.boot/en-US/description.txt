The Termux:Boot add-on provides functionality to run programs under Termux when the device boots up.

Usage:

- Start the Termux:Boot app once by clicking on its launcher icon. This allows the app to be run at boot.
- Create the ~/.termux/boot/ directory.
- Put scripts you want to execute inside the ~/.termux/boot/ directory.
- If there are multiple files, they will be executed in a sorted order.
- Note that you may want to run termux-wake-lock as first thing if you want to ensure that the device is prevented from sleeping.

Example:
To start an sshd server and prevent the device from sleeping at boot, create a file at ~/.termux/boot/start-sshd containing the two lines:

termux-wake-lock
sshd

Join the Termux Google+ community:
https://termux.com/community

Report issues:
https://github.com/termux/termux-boot/issues
