The Kali NetHunter is an Android ROM overlay that includes a robust Mobile Penetration Testing Platform. The overlay includes a custom kernel, a Kali Linux chroot, and an accompanying Android application, which allows for easier interaction with various security tools and attacks. Beyond the penetration testing tools arsenal within Kali Linux, NetHunter also supports several additional classes, such as HID Keyboard Attacks, BadUSB attacks, Evil AP MANA attacks, and much more. For more information about the moving parts that make up NetHunter, check out our NetHunter Components page. NetHunter is an open-source project developed by Offensive Security and the community.

# Kali NetHunter Application

* Home Screen - General information panel, network interfaces and HID device status.
* Kali Chroot Manager - For managing chroot metapackage installations.
* Check App Update - For checking Kali NetHunter Android App updates.
* Kali Services - Start / stop various chrooted services. Enable or disable them at boot time.
* Custom Commands - Add your own custom commands and functions to the launcher.
* MAC Changer - Change your Wi-Fi MAC address (only on certain devices)
* KeX Manager - Set up an instant KeX session with your Kali chroot.
* HID Attacks - Various HID attacks, Teensy style.
* DuckHunter HID - Rubber Ducky style HID attacks
* BadUSB MITM Attack - Nuff said.
* MANA Wireless Toolkit - Setup a malicious Access Point at the click of a button.
* MITM Framework - Inject binary backdoors into downloaded executables on the fly.
* NMap Scan - Quick Nmap scanner interface.
* Metasploit Payload Generator - Generating Metasploit payloads on the fly.
* Searchsploit - Easy searching for exploits in the Exploit-DB.

Requires root, nethunter chroot and nethunter kernel
 
This app is built and signed by Kali NetHunter.
